## The Quay X.509 Certificate Authority

This project consists of the configuration and support scripts I use to run a
small private X.509/TLS certificate authority (CA) for my internal use.

Generally I recommend that most users investiage
[Let's Encrypt](https://letsencrypt.org) to see if it's suitble for their
purposes.  It is widely supported in nearly all web browsers, easy to use, and
provides a much simpler integration from an end user standpoint.  The primary
limitation of Let's Encrypt is that it is difficult to use with systems that
are not exposed to the Internet, hence my use case.

For additional information you can visit my
[wiki](https://quay.net/wiki/crypto:x509).

One final reminder: this is a bit hacky and isn't designed to scale for
situations such as corporate use, but it's more than workable for a small
private network or for use in a lab environment.

### Contents

[[_TOC_]]

### TODO

* Reference info for how to install and use the CA cert on clients
* Notes for using a certificate chain with various web servers and systems
* Implement elliptic curve CA

## A basic tutorial for using this scheme

Here you will find some documentation on how I use this structure to manage my
local CA.  The basic configuration is fairly modern and reasonably sane, but I
do not work with in PKI or crypto in my day job any longer, so please be careful
before applying this process in a critical environment.

### Before you begin

:bangbang: **WARNING: Protect your private keys!**

As always, you need to be sure that you protect your private keys.  Minimally
this means that you are doing the following:

1. Use cryptographically strong passwords for the CA keys, if they become
exposed for any reason then a malicious actor *can impersonate you* without
you being any the wiser.
1. Your keys should be excluded from any Git repo (including this one) as they
will still exist in Git history even if they ar deleted from the current
working version.
1. This scheme implements the best practice of using a separate root and
itermediate (certificate signing) CA. Generally, the point of using an
intermediate signing certificate is to *completely* offline your root key. So 
consider storing it securely in an offline location (a USB key, in a safe,
surrounded by lava, guarded by rabid honey badgers) and only bring it out to
sign or revoke the intermediate signing certs and to update your CRL.

The key sizes and formats found in my configuration should be considered the
*minimum* acceptable options in 2020.

By default the ```.gitignore``` rules of this project ignore all files that end
with ```.key``` or are located in the respective ```/private``` directories of
each CA and sub-CA. Be mindful of this configuration when using the CA with
Git. *And to reiterate, this risk is substantially mitigated by keeping bullet
3 in mind.*

*Be careful! You have been warned.*

### Overview

The creation and management of the CA is divided into several sections.  The
graph below provides a general overview of the workflow from initialization of
the CA to the creation and signature of your first user (server) certificate.

#### Workflow

```mermaid
graph TB

  IntEnv1-.->UsrEnv;

  subgraph "Optional: create user (server) certificate"
  UsrEnv([Set user certificate environment])-->GenUsrKey;
  GenUsrKey[Generate user cert key]-->GenUsrCert;
  GenUsrCert[Generate user cert CSR];
  end

  SelfSignRoot-->IntEnv0;
  SignIntCsr-->IntEnv1;
  GenUsrCert-->SignUsrCert;

  subgraph "Intermediate CA (signing cert)"
  IntEnv0([Configure signing cert environment])-->GenIntKey;
  GenIntKey[Generate signing cert key]-->GenIntCert;
  GenIntCert[Generate signing cert CSR];
  IntEnv1([Switch to signing cert environment]);
  IntEnv1-->GenIntCrl;
  SignUsrCert[Sign user cert CSR]-->GenIntCrl;
  GenIntCrl[Generate signing cert CRL];
  end

 GenIntCert-->RootEnv1;

  subgraph "Root CA"
  RootEnv0([Configure root CA environment])-->GenRootKey;
  GenRootKey[Generate root CA key]-->GenRootCert;
  GenRootCert[Generate root certificate]-->SelfSignRoot;
  SelfSignRoot[Self-sign root certificate];
  RootEnv1([Switch to root CA environment])-->SignIntCsr;
  SignIntCsr[Sign intermediate CA CSR]-->GenRootCrl;
  GenRootCrl[Generate root CA CRL]-->StoreRootKey;
  StoreRootKey[Store root CA key offline];
  end

```

### Before you begin

If you are not familiar with [OpenSSL](https://www.openssl.org), it's a good
idea to review the documentation for the ```openssl ca``` command that [comes
with your system](https://www.openssl.org/docs/man1.1.1/man1/ca.html) as
the options sometimes change between versions. This documentation is currently
based on OpenSSL 1.1.1 as shipped with Debian 10.

The only other dependency is a version of
[GNU Bash](https://www.gnu.org/software/bash/) from the 21st century.

If you are cloning this Git project, delete the contents of the following
directories to remove my site specific data: ```root-q2```, ```sign-q2```,
```user```, and ```cacerts```.  You will recreate the required structure when
you execute the steps found below.  You may optionally wish to reinitialize
the git repo and delete my history to save space.

Examine the contents of [```conf/caenv.env```](conf/caenv.env) and customize
the paths as you see fit.  Keep in mind that you may need to modify the
configuration of the OpenSSL config files (```conf/root-q2.conf``` and
```conf/sign-q2.conf```) if you do.

### Step 1. Creating the root CA

The first step of generating any CA is to self-sign your root certificate. It
is therefore important that you review and customize the OpenSSL config files
found in the [```/conf```](conf/root-q2.conf).  In particular the ```$dir```
variable is unlikely to be correct for your system. I've made some effort to
comment the options in-line, where their use is not obvious.

Creating the root CA assets involves the following tasks:

1. generate the root CA private key
1. create a CSR with the necessary information for a CA
1. self-sign the CSR using the private key you just generated

The commands I use to create the root CA and comments can be found here:
[Creating the root certificate authority](docs/create-root.md)

### Step 2. Creating an intermediate (signing) CA

The intermediate CA will be used to sign user certifcates.  The process is
similar to creating the root CA, except the CSR will be signed using the
certificate and key you generated in the previous step.

1. generate the intermediate CA private key
1. create a CSR for the intermediate CA
1. sign the CSR using your root CA with the ```basicConstraints``` set to
```CA:true```

The detailed commands to perform these tasks can be found here:
[Creating the intermediate certificate authority](docs/create-sign.md)

### Step 3. Signing your first user certificate

Creating and signing a certificate follows from the previous step, with your
user CSR being signed by the intermediate CA.

1. generate user certificate key
1. create CSR for your user certificate (make sure to specify a
```subjectAltName``` extension)
1. sign the CSR using your intermediate CA

The commands to generate your certificate can be found here:
[Creating and signing user (server) certificates](docs/user-certs.md).

### Revoking certificates and managing the CRL

X.509 certificates are revoked by publicly publishing a CRL file (the location
of which is specified when you originally created the CA certificate with the
```crlDistributionPoints``` extension).  This file is created using the
```openssl crl``` command and signed with the CA cert that originally signed
the certificate that you wish to revoke.

The CRL should be updated on a regular basis in the case of a publicly used CA,
but for private use, this is less critical.  Note that the current best practice
is to use the [Online Certificate Status Protocol 
OCSP](https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol) for
managing certificate revocation status with X.509.  But this is a much more
complex method than is generally justified for the scenarios that are
contemplated in these instructions.  OpenSSL [implements a basic OCSP
responder](https://www.openssl.org/docs/man1.1.1/man1/ocsp.html)
via the ```openssl ocsp``` command, but this is not a suitable solution for
production use.

In general, I will update my CRL every time I generate a new certificate or
once every 90 days (whichever comes first).

See: [Managing the CRL and revoking certificates](docs/revoke-cert.md)

### Certificate authority management

This is incomplete, visit [my wiki](https://quay.net/wiki/crypto:x509)
for my current notes.

## OpenSSL cheat sheet

```bash
# show the contents of a certificate
openssl -noout -text -in <cert>

# check the validity of a certificate chain
openssl verify -CAfile <signing cert or cert chain> <cert>

# change password your key file
openssl rsa -aes256 -in <current key> -out <new key>
```

## Reference

* https://www.openssl.org/docs/man1.1.1/man1/ca.html
* https://quay.net/wiki/crypto:x509?rev=1551125915
* https://www.phildev.net/ssl/managing_ca.html
* https://www.erianna.com/ecdsa-certificate-authorities-and-certificates-with-openssl/
* https://www.digicert.com/kb/ssl-support/openssl-quick-reference-guide.htm

### Shield Icon License

This project uses a modified version of the
[Shield PNG Icon](https://www.pngrepo.com/svg/39572/shield)
under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.

