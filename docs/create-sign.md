## Creating the intermediate certificate authority

These steps will generate an intermediate CA certificate and key.  You will
require the CA root certificate and key you created in the
[previous step](create-root.md).

### Environment configuration notes

You will likely wish to modify the ```$dir``` variable in
[```conf/sign-q2.conf```](conf/sign-q2.conf).  You should also examine this
configuration file it will be set as ```$OPENSSL_CONF``` in the next step.

The configuration of the intermediate CA and root CA are kept in separate
files in order to avoid the possibility of inadvertently applying
options that are intended only for signing sub-CA certificates when using
the signing CA.  It's entirely possible for these configurations to all be
contained in the same file, provided the environment is adjusted accordingly.

All commands are run from the root of this project.

#### Set some variables
```bash
export ROOT_CA=root-q2
export SIGN_CA=sign-q2
export CA=$SIGN_CA # note that we're using the $SIGN_CA name as $CA
source conf/caenv.env
```

### Generate intermediate certificate

This is the key you will eventually use to sign server certs.  It should be
handled with care, although you can revoke the intermediate CA, it's not only
a huge hassle, most clients will not routinely be validating that certs you
issue have not been revoked and configuring
[OCSP](https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol)
is well beyond the scope of setting up a simple CA for local use.


```bash
openssl genrsa -aes256 -out $CA_PRIVATE_KEY 2048
chmod 600 $CA_PRIVATE_KEY
```

### Generate certificate request for intermediate cert

Generate your CSR for the intermediate CA root.  As before, you should
modify the subject to suit your needs, you can also remove this line and
OpenSSL will prompt you to provide values interactively.

```bash
openssl req \
  -new \
  -subj "/C=CA/ST=Ontario/L=Toronto/O=The\ Quay/CN=The Quay Intermediate Q2" \
  -extensions v3_ca_sign \
  -key $CA_PRIVATE_KEY \
  -out $CA_WORK_DIR/csr/${CA}.csr
```

### Verify the CSR

Make sure you like what you see here.

```bash
openssl req -verify -noout -text -nameopt multiline -in $CA_WORK_DIR/csr/${CA}.csr
```

#### Reset the environment to use the root CA

Now return to root CA environment to sign intermediate cert.


```bash
export CA=$ROOT_CA
source conf/caenv.env
```

### Sign the intermediate cert with the CA

The intermediate CA is signed using the self-signed cert and key you generated
in the previous stage.

```bash
openssl ca \
  -extensions v3_ca_sign \
  -startdate 200321000000Z \
  -enddate 291231000000Z \
  -in $CA_ROOT_DIR/$SIGN_CA/csr/$SIGN_CA.csr \
  -out $CA_ROOT_DIR/$SIGN_CA/certs/$SIGN_CA.crt
```

### Verify intermediate certificate

Make sure that the intermediate certificate is configured correctly as it will
be used to sign all of your other certificates.  If there are problems, you
should start over, or simply revoke this certificate and regenerate it.

```bash
openssl x509 -noout -text -in $CA_ROOT_DIR/$SIGN_CA/certs/$SIGN_CA.crt
```

### Verify signature chain

Check to make sure that the signature chain is valid.

```bash
openssl verify -CAfile $CA_ROOT_DIR/$ROOT_CA/certs/$ROOT_CA.crt \
  $CA_ROOT_DIR/$SIGN_CA/certs/$SIGN_CA.crt
```

You can now use this CA to [sign your user certificates](user-certs.md).

### Optional: Create CA cert chain for use in webservers and copy CA certs cacerts dir

This step is not necessary, but it is convenient for distributing and using
the certs on other systems.

```bash
cp $CA_ROOT_DIR/$SIGN_CA/certs/$SIGN_CA.crt $CA_ROOT_DIR/cacerts
cp $CA_ROOT_DIR/$ROOT_CA/certs/$ROOT_CA.crt $CA_ROOT_DIR/cacerts

cat $CA_ROOT_DIR/$SIGN_CA/certs/$SIGN_CA.crt \
  $CA_ROOT_DIR/$ROOT_CA/certs/$ROOT_CA.crt > \
  cacerts/quay-cert-chain.crt
```
