## Managing the CRL and revoking certificates

It's a good idea to update your certificate revocation list from time to time,
at a minimum it should be published before the expiration of the current CRL.

In this case because this is a private CA with little exposure outside of my
network the publication frequency is set to 90 days.  But a more widely used
CA should probably use a frequency of 30 days or less.

### Set some variables

As always, let's configure our environment.

```bash
export ROOT_CA=root-q2
export SIGN_CA=sign-q2
export CA=$SIGN_CA
source conf/caenv.env
```

### Show all certificates signed by our CA

This will show us the list of certificates that we have signed.

```bash
cat $CA_WORK_DIR/meta/index.txt
```
By default, every cert we create with the CA is stored in
```$CA_WORK_DIR/certs``` with the file name ```<serial number>.pem```.

### Revoking a certificate

When revoking a certificate we should give a reason which must be one of the
following values:

```
unspecified
keyCompromise
CACompromise
affiliationChanged
superseded
cessationOfOperation
certificateHold
```

In our case the most likely reasons are going to be ```keyCompromise``` which
is pretty self explanatory, and ```superseded``` which I've used when
updating or fixing a misconfigured certificate.

The actual command is pretty straightforward:

```bash
openssl ca -crl_reason superseded \
  -revoke $CA_WORK_DIR/certs/0E692634F4FF87A39AE3DFB24389535A53D13384.pem
```

### Generate new CRL

Now generate the CRL itself.

```bash
openssl ca \
  -gencrl \
  -out $CA_WORK_DIR/crl/$CA.crl
```

### Publish CRL helper script

This CRL must now be published to the location specified in
```crlDistributionPoints```.  I use the following simple script which you
can modify for your local environment.

```bash
./bin/publish-crl.sh
```
