## Creating the root certificate authority

These steps will generate a CA root certificate and key using the structure found
in this project.

### Environment configuration notes

To simplify the process of managing the CA a number of environment variables
are expected to be set to the appropriate values. These paths are expected by
the OpenSSL configuration files used to manage the CA.

You will likely wish to modify the ```$dir``` variable in
[```conf/root-q2.conf```](conf/root-q2.conf).  You should also examine this
configuration file it will be set as ```$OPENSSL_CONF``` in the next step.

All commands are run from the root of this project.

#### Set some variables
```bash
export ROOT_CA=root-q2
export SIGN_CA=sign-q2
export CA=$ROOT_CA
source conf/caenv.env
```

### Generate directory structre for root CA

This script needs to be run once in order to build the empty directory
structure that OpenSSL expects.


```bash
./bin/mkca.sh
```

### Generate private key for root CA

Now we will generate our private key for the root CA.  Remember to keep this
file safe, unauthorized access to this key will allow an attacker to issue
signing certs that will be trusted by any system that is configured to trust
your root.


```bash
openssl genrsa -aes256 -out $CA_PRIVATE_KEY 2048
chmod 600 $CA_PRIVATE_KEY
```

### Generate self-signed CA signing request for root CA

Generate your certificate signing request (CSR) for the CA root.  You should
modify the subject to suit your needs, you can also remove this line and
OpenSSL will prompt you to provide values interactively.

```bash
openssl req \
  -new \
  -subj "/C=CA/ST=Ontario/L=Toronto/O=The\ Quay/CN=The Quay CA Root Q2" \
  -extensions v3_ca_root \
  -key $CA_PRIVATE_KEY \
  -out $CA_WORK_DIR/csr/${CA}.csr
```

### Verify the CSR

Check to make sure that the CSR looks correct before you sign it.  After the
next step you will be committed to these values.

```bash
openssl req -verify -noout -text -nameopt multiline -in $CA_WORK_DIR/csr/${CA}.csr
```

### Self-sign CSR for root CA

By definition a root CA is self-signed.  This CA will be valid from March 21,
2020 until December 30, 2029.  Note that all dates and times used by TLS are
specified in UTC.

```bash
openssl ca -selfsign \
  -create_serial \
  -extensions v3_ca_root \
  -startdate 200321000000Z \
  -enddate 291231000000Z \
  -in $CA_WORK_DIR/csr/${CA}.csr \
  -out $CA_CERT
```

If/when you run the ```openssl ca``` command in the future you can omit the
```-create_serial``` flag.

### Verify certificate

Now verify that you like what you see before you proceed to the generate your
intermediate CA certificate

```bash
openssl x509 -noout -text -in $CA_CERT
```
