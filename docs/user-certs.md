## Creating and signing user (server) certificates

The following procedure can be used to create a user CSR and sign it with the 
intermediate CA you created in [step 2](create-sign.md).

### Environment configuration notes

All user certs will be signed with our intermediate CA, so it is important
to ensure that the appropriate options are configured in
[```conf/sign-q2.conf```](conf/sign-q2.conf) as it will be used as
```$OPENSSL_CONF```.

All commands are run from the root of this project.

#### Set some variables
```bash
SIGN_CA=sign-q2
export CERT_NAME=router.in.quay.net
export CA=$SIGN_CA # this must be set to the name of the intermediate CA
source conf/caenv.env
```

### Create directories to store user certificate assets

You will need to generate a directory structure to store your user keys and
CSRs.

```bash
mkdir -p user/{certs,private,csr}
```

### Generate a private key for your certificate

This key probably shouldn't have a passphrase as it will most likely be used
in a non-interactive mode by daemons that run as root.  If this key is exposed
you should [revoke](revoke-cert.md) the certficiate.

```bash
openssl genrsa -out $USER_KEY
```

### Generating the CSR for your user certificate

To ensure that your certificates will work with all browsers it is necessary
to add the ```subjectAltName``` (SAN) extension to your certificate with the same
value as the ``CN`` value of your subject regardless of whether or not you
require multiple subjects.  Therefore there are instructions for both modes
below.

#### Generate a standard single hostname CSR with SAN

This will work becasue ```subjectAltName``` is configured to copy the value
of ```CN``` in the configuration file.

```bash
openssl req \
  -new \
  -subj "/O=The\ Quay/CN=$CERT_NAME" \
  -addext "subjectAltName = DNS:$CERT_NAME" \
  -key $USER_KEY \
  -out $USER_CSR
```

#### Generate a CSR with multiple SAN

In this example we are generating a wildcard certificate for the in.quay.net domain.

```bash
export CERT_NAME=in.quay.net

openssl req \
  -new \
  -subj "/O=The\ Quay/CN=$CERT_NAME" \
  -addext "subjectAltName = DNS:$CERT_NAME, DNS:*.$CERT_NAME" \
  -key $USER_KEY \
  -out $USER_CSR
```

This is the easiest way to handle this scenario as it is impossible to specify
multiple values for ```subjectAltName``` interactively using ```openssl req```.

### Verify the CSR

Now verify your CSR.

```bash
openssl req -verify -noout -text -nameopt multiline -in $USER_CSR
```

### Sign the CSR using the intermediate CA

You can omit ```-create_serial``` after first run.

```bash
openssl ca \
  -create_serial \
  -startdate `date +%y%m%d000000Z -u -d -1day` \
  -enddate `date +%y%m%d000000Z -u -d +3years` \
  -infiles $USER_CSR
```

### Copy cert to user directory

Your new certificate will be saved to CA directory as ```certs/<SERIAL>.crt``` and
can be copied to the user directory for ease of use.

```bash
cp -f $SIGN_CA/certs/<SERIAL> $USER_CERT

# verify user cert
openssl verify -CAfile $CA_ROOT_DIR/cacerts/quay-cert-chain.crt \
  $USER_CERT
```
Congratulations, you've signed your first certificate!  Make sure to review
[the documentation](revoke-cert.md) on revoking certificates and managing CRLs.
