#!/bin/bash

mkdir -p ${CA_WORK_DIR}/{certs,crl,csr,newcerts,private,meta}
chmod 700 ${CA_WORK_DIR}/private
touch ${CA_WORK_DIR}/meta/index.txt
# serial is created by openssl during CA creation
#openssl rand -hex 16 > ${CA_WORK_DIR}/meta/serial.txt
touch ${CA_ROOT_DIR}/.random
