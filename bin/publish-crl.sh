#!/bin/bash
#
# publish CRL to our server
#

SERVER=calculon
CRL_PATH=/var/www/vhosts/quay.net/pub/ca

scp $CA_WORK_DIR/crl/$CA.crl $SERVER:$CRL_PATH/
ssh $SERVER chmod 644 $CRL_PATH/$CA.crl


