#!/bin/bash

# generate key
openssl genrsa -aes256 -out $CA_PRIVATE_KEY 2048
chmod 600 $CA_PRIVATE_KEY

# generate certificate request
openssl req -newkey rsa:2048 -x509 \
  -days 4242 \
  -create_serial \
  -selfsign \
  -key $CA_PRIVATE_KEY \
  -sha256 \
  -extensions v3_ca \
  -out $CA_CERT

